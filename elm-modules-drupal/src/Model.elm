module Model exposing (..)

---- MODEL ----

import RemoteData


type alias Model =
    { modules : RemoteData.WebData (List Module)
    , nameFilter : String
    , messages : List String
    , modulesEnableQueue : List String
    }


type alias Module =
    { name : String
    , humanName : String
    , description : Maybe String
    , package : Maybe String
    , dependencies : List String
    , enabled : Bool
    , helpUrl : Maybe String
    , configUrl : Maybe String
    , permissionUrl : Maybe String
    }


type Msg
    = ModulesLoadingResult (RemoteData.WebData (List Module))
    | ModuleEnable String
    | ModulesEnableResult (RemoteData.WebData String)
    | ChangeNameFilter String


